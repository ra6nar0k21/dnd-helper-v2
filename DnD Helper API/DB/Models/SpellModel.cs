using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DnD_Data.Formats;

namespace DnD_Helper_API.DB.Models;

public class SpellModel
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public long Id { get; set; }

    [Required]
    [Column("name_german")]
    [StringLength(256)]
    public string NameGerman { get; set; }

    [Required]
    [Column("name_english")]
    [StringLength(256)]
    public string NameEnglish { get; set; }

    public int TimeConsumptionValue { get; set; }
}