using DnD_Helper_API.DB.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace DnD_Helper_API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SpellListController : Controller
{
    private readonly ApiContext _context;

    public SpellListController(ApiContext context)
    {
        _context = context;
        _context.Database.EnsureCreated();
    }
    
    // GET
    [HttpGet("all")]
    public IActionResult GetAllSpells()
    {
        _context.Spells.AddRange(
        new SpellModel{
            NameEnglish = "Test",
            NameGerman = "Test",
        });
        _context.SaveChanges();
        return Ok();
    }

    // Gets all translations
    [HttpGet("translations")]
    public IActionResult GetTranslations()
    {
        return Ok();
    }
}