using System.ComponentModel;
using System.Reflection;
using DnD_Data.Attributes;

namespace DnD_Data;

public static class Utils
{
    public static string Pluralize(int value, string text)
    {
        return value == 1 ? text : $"{text}n";
    }
}