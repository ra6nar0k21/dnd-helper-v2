using System.ComponentModel;
using DnD_Data.Attributes;

namespace DnD_Data.Formats;

public enum TimeConsumptionFormat
{
    [EnumTranslation("Aktion")]
    Action,
    [EnumTranslation("Bonus Aktion")]
    BonusAction,
    [EnumTranslation("Stunde")]
    Hour,
    [EnumTranslation("Minute")]
    Minute,
    [EnumTranslation("Bis zu ... Minute")]
    UpToMinute,
    [EnumTranslation("Bis zu ... Stunde")]
    UpToHour,
    [EnumTranslation("Ende der Runde")]
    EndOfRound,
    [EnumTranslation("Runde")]
    Round,
    [EnumTranslation("Reaktion")]
    Reaction
}