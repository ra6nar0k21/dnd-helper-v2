namespace DnD_Data.Attributes;

[AttributeUsage(AttributeTargets.Field)]
public class EnumTranslationAttribute : Attribute
{
    public EnumTranslationAttribute(string translation)
    {
        Translation = translation;
    }

    public string Translation { get; }
}

public static class EnumTranslation
{
    public static string Get<T>(T enumerator)
        where T : struct
    {
        var type = enumerator.GetType();
        if (!type.IsEnum)
        {
            throw new ArgumentException("EnumerationValue must be of Enum type", nameof(enumerator));
        }

        var memberInfo = type.GetMember(enumerator.ToString() ?? string.Empty);
        if (memberInfo.Length > 0)
        {
            var attrs = memberInfo[0].GetCustomAttributes(typeof(EnumTranslationAttribute), false);
            if (attrs.Length > 0)
            {
                return ((EnumTranslationAttribute) attrs[0]).Translation;
            }
        }

        return enumerator.ToString() ?? "";
    }
}