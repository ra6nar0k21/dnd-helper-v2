namespace Localization;

public static class Localizations
{
    private static readonly Dictionary<string, LocalizationData> LocalizationsDict = new();

    private static LocalizationData? _active;

    private static LocalizationData _defaultLocalization;

    public static void LoadLocalizations(string path)
    {
        if (!Directory.Exists(path))
        {
            return;
        }

        foreach (var file in Directory.GetFiles(path))
        {
            if (!file.EndsWith(".lang"))
            {
                continue;
            }

            LocalizationsDict.Add(file[..file.LastIndexOf('.')], LocalizationData.FromText(File.ReadAllLines(file)));
        }
    }

    public static void AddLocalization(string name, LocalizationData data)
    {
        LocalizationsDict.Add(name, data);
    }

    public static LocalizationData GetLocalization(string name)
    {
        if (LocalizationsDict.ContainsKey(name))
        {
            return LocalizationsDict[name];
        }

        return _defaultLocalization;
    }

    public static LocalizationData Current
    {
        get { return _active ??= _defaultLocalization; }
    }

    public static void SelectLocalization(string name)
    {
        if (!LocalizationsDict.ContainsKey(name))
        {
            _active = _defaultLocalization;
            return;
        }

        _active = LocalizationsDict[name];
    }

    public static LocalizationData DefaultLocalization
    {
        get => _defaultLocalization;
        set => LocalizationsDict.Add("default", value);
    }

    public static string GetLocalizationForKey(LocalizationData localization, string key)
    {
        return localization.GetFromKey(key);
    }

    public static string GetLocalizationForKey(string key)
    {
        return Current.GetFromKey(key);
    }
}