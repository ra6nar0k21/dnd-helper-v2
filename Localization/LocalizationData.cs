using System.Text.Json;
using System.Text.Json.Nodes;

namespace Localization;

public class LocalizationData
{
    private readonly Dictionary<string, string> _localization = new();

    public static LocalizationData FromText(string[] data)
    {
        var local = new LocalizationData();

        foreach (var k in data)
        {
            if (k.Trim().Length <= 0)
            {
                continue;
            }

            if (!k.Contains(':'))
            {
                continue;
            }
            
            var args = k.Trim().Split(":");
            local.AddTranslation(args[0], args[1]);
        }

        return local;
    }

    public string GetFromKey(string key)
    {
        if (_localization.ContainsKey(key))
        {
            return _localization[key];
        }

        return key;
    }

    public Dictionary<string, string> Locals => _localization;

    public void AddTranslation(string key, string translation)
    {
        _localization.Add(key, translation);
    }
}