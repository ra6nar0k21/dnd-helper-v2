using Gtk;

namespace DnD_Helper.Extensions;

public static class WidgetExtension
{
    public static T FindChild<T>(this Container widget, string childName) where T : Widget
    {
        while (true)
        {
            if (widget is Bin bin)
            {
                if (bin.Name == childName)
                {
                    return bin as T;
                }

                if (bin.Child.Name == childName)
                {
                    return bin.Child as T;
                }

                if (bin.Child is Container child)
                {
                    widget = child;
                    continue;
                }
            }

            foreach (var w in widget.Children)
            {
                if (w.Name == childName)
                {
                    return w as T;
                }

                if (w is Container container)
                {
                    return FindChild<T>(container, childName);
                }
            }

            return null;
        }
    }
}