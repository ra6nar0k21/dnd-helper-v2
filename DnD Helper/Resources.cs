using System.IO;
using System.Linq;
using System.Reflection;

namespace DnD_Helper;

public static class Resources
{
    public static string RootPath { get; private set; }

    public static string RaceFile { get; private set; }

    public static void Init()
    {
        RootPath = $"{Assembly.GetExecutingAssembly().Location[..System.Reflection.Assembly.GetExecutingAssembly().Location.LastIndexOf(Path.DirectorySeparatorChar)]}{Path.DirectorySeparatorChar}data";
        Directory.CreateDirectory(RootPath);

        RaceFile = $"{RootPath}{Path.DirectorySeparatorChar}races.json";
    }

    /// <summary>
    /// Reads a internal asset to string
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public static string ReadInternal(string name)
    {
        // Determine path
        var assembly = Assembly.GetExecutingAssembly();
        var resourcePath = $"DnD_Helper.{name}";
        // Format: "{Namespace}.{Folder}.{filename}.{Extension}"
        if (!name.StartsWith(nameof(Program)))
        {
            resourcePath = assembly.GetManifestResourceNames()
                .Single(str => str.EndsWith(name));
        }

        using var stream = assembly.GetManifestResourceStream(resourcePath);

        if (stream == null)
        {
            return "";
        }

        using var reader = new StreamReader(stream);
        return reader.ReadToEnd();
    }
}