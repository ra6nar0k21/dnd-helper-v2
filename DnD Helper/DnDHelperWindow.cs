using System.Collections.Generic;
using DnD_Helper.Components;
using DnD_Helper.Components.List;
using DnD_Helper.Components.PointBuy;
using DnD_Helper.Components.TranslationCreator;
using Gtk;
using Localization;

namespace DnD_Helper;

public class DnDHelperWindow : Window
{
    public DnDHelperWindow() : base(WindowType.Toplevel)
    {
        Resize(1280, 720);
        SetSizeRequest(1280, 720);
        // Quits when window is deleted
        DeleteEvent += (o, eventArgs) => Application.Quit();

        Populate();
    }

    private void Populate()
    {
        var notebook = new Notebook
        {
            TabPos = PositionType.Left
        };

        List<IComponent> components = new()
        {
            new PointBuy(),
            new List(),
        };

        if (Program.DebugMode)
        {
            components.Add(new TranslationCreator());
        }

        foreach (var component in components)
        {
            notebook.AppendPage(component.BuildWidget(this), Label.New(Localizations.GetLocalizationForKey(component.ComponentTitleLocalizationKey)));
        }

        Add(notebook);
    }
}