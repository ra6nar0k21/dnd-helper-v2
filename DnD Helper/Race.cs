using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using Newtonsoft.Json;

namespace DnD_Helper;

[JsonObject(MemberSerialization.OptIn)]
public class Race
{
    [JsonProperty] public string Name { get; init; }
    [JsonProperty] public int StrengthBonus { get; init; } = 0;
    [JsonProperty] public int DexterityBonus { get; init; } = 0;
    [JsonProperty] public int ConstitutionBonus { get; init; } = 0;
    [JsonProperty] public int IntelligenceBonus { get; init; } = 0;
    [JsonProperty] public int WisdomBonus { get; init; } = 0;
    [JsonProperty] public int CharismaBonus { get; init; } = 0;
}

public static class Races
{
    private static Dictionary<string, Race> _races = new();

    public static Race GetRace(string name)
    {
        return !_races.ContainsKey(name) ? null : _races[name];
    }

    public static Dictionary<string, Race> AllRaces => _races;

    public static void AddRace(Race race)
    {
        _races.Add(race.Name.ToLower(), race);
    }
}

public static class RaceLoader
{
    /// <summary>
    /// Saves the internal race file
    /// </summary>
    /// <param name="jsonPath"></param>
    /// <returns></returns>
    private static IEnumerable<Race> SaveDefaultRaces(string jsonPath)
    {
        var serializer = new JsonSerializer();
        var defaults =
            serializer.Deserialize<Race[]>(
                new JsonTextReader(new StringReader(Resources.ReadInternal("Assets.races.json"))));

        using var sw = new StreamWriter(jsonPath!);
        using var writer = new JsonTextWriter(sw);
        serializer.Serialize(writer, defaults);

        return defaults;
    }

    /// <summary>
    /// Loads races from a given path
    /// </summary>
    /// <param name="jsonPath"></param>
    /// <param name="replace"></param>
    /// <returns></returns>
    public static IEnumerable<Race> LoadRaces(string jsonPath, bool replace = false)
    {
        if (!File.Exists(jsonPath) || replace)
        {
            if (File.Exists(jsonPath))
            {
                File.Delete(jsonPath);
            }

            SaveDefaultRaces(jsonPath);
        }

        var serializer = new JsonSerializer();
        var result = serializer.Deserialize<Race[]>(new JsonTextReader(new StringReader(File.ReadAllText(jsonPath!))));

        foreach (var race in result!)
        {
            Races.AddRace(race);
        }

        return result;
    }
}