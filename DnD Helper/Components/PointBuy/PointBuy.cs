#nullable enable
using System;
using System.Collections.Generic;
using Gtk;
using Localization;

namespace DnD_Helper.Components.PointBuy;

internal delegate void CallBack();

internal delegate void PointCostChangedEventHandler(object sender);

internal struct AttributeWidget
{
    public Widget[] Widgets { get; init; }

    public CallBack Reset { get; init; }
    public CallBack ResetRacialBonus { get; init; }
}

public class PointBuy : IComponent
{
    public string CssClassPrefix => "pointbuypage";
    public string ComponentLocalizationKey => "page.pointbuy";
    public string ComponentTitleLocalizationKey => $"{ComponentLocalizationKey}.title";

    private const int AbilityMin = 8;
    private const int AbilityMax = 15;

    private const int MaxPoints = 27;

    private event PointCostChangedEventHandler? PointCostChangedEvent;

    private readonly Dictionary<int, int> _costs = new()
    {
        {8, 0},
        {9, 1},
        {10, 2},
        {11, 3},
        {12, 4},
        {13, 5},
        {14, 7},
        {15, 9}
    };

    private readonly Dictionary<string, int> _abilityCosts = new()
    {
        {"strength", 0},
        {"dexterity", 0},
        {"constitution", 0},
        {"intelligence", 0},
        {"wisdom", 0},
        {"charisma", 0}
    };

    private Race? _race = null;
    private bool IsCustomRace => _race == null;

    private int RemainingPoints
    {
        get
        {
            var result = 0;
            foreach (var (k, value) in _abilityCosts)
            {
                result += value;
            }

            return result;
        }
    }

    public Widget BuildWidget(Widget parent)
    {
        var box = new Box(Orientation.Horizontal, 6);

        var grid = new Grid();
        grid.Halign = Align.Center;
        grid.Valign = Align.Center;

        grid.ColumnSpacing = 6;
        grid.RowSpacing = 6;

        string[] titles = {$"{ComponentLocalizationKey}.attribute.title", $"{ComponentLocalizationKey}.abilityscore.title", "+", $"{ComponentLocalizationKey}.racialbonus.title", "=", $"{ComponentLocalizationKey}.totalscore.title", $"{ComponentLocalizationKey}.abilitymodifier.title", $"{ComponentLocalizationKey}.pointcost.title"};
        for (var i = 0; i < titles.Length; i++)
        {
            var label = Label.New(Localizations.GetLocalizationForKey(titles[i]));
            label.StyleContext.AddClass($"{CssClassPrefix}-attribute_label__title");
            grid.Attach(label, i, 0, 1, 1);
        }

        var strength = BuildAttributeWidget("strength");
        var dexterity = BuildAttributeWidget("dexterity");
        var constitution = BuildAttributeWidget("constitution");
        var intelligence = BuildAttributeWidget("intelligence");
        var wisdom = BuildAttributeWidget("wisdom");
        var charisma = BuildAttributeWidget("charisma");

        void AddElement(int index, IReadOnlyList<Widget> widgets)
        {
            for (var i = 0; i < widgets.Count; i++)
            {
                grid.Attach(widgets[i], i, index, 1, 1);
            }
        }

        AddElement(1, strength.Widgets);
        AddElement(2, dexterity.Widgets);
        AddElement(3, constitution.Widgets);
        AddElement(4, intelligence.Widgets);
        AddElement(5, wisdom.Widgets);
        AddElement(6, charisma.Widgets);

        var resetButton = Button.NewWithLabel(Localizations.GetLocalizationForKey($"{ComponentLocalizationKey}.reset.title"));


        resetButton.Pressed += (sender, args) =>
        {
            strength.Reset();
            dexterity.Reset();
            constitution.Reset();
            intelligence.Reset();
            wisdom.Reset();
            charisma.Reset();
        };

        grid.Attach(resetButton, 1, 7, 1, 1);

        var pointCostLabel = Label.New($"{RemainingPoints}/{MaxPoints}");
        pointCostLabel.StyleContext.AddClass($"{CssClassPrefix}-attribute_label");
        PointCostChangedEvent += (sender) => { pointCostLabel.Text = $"{RemainingPoints}/{MaxPoints}"; };
        grid.Attach(pointCostLabel, strength.Widgets.Length - 1, 7, 1, 1);

        var raceList = new ListStore(typeof(int), typeof(string), typeof(string));
        var index = 1;
        raceList.AppendValues(0, "Custom");
        foreach (var (raceIdName, race) in Races.AllRaces)
        {
            raceList.AppendValues(index, race.Name, raceIdName);
            index++;
        }

        var races = ComboBox.NewWithModelAndEntry(raceList);
        races.EntryTextColumn = 1;
        races.Active = 0;

        var lastActive = races.Active;

        races.Changed += (sender, args) =>
        {
            if (races.Active < 0)
            {
                races.Active = lastActive;
            }

            if (races.Active == 0)
            {
                lastActive = 0;
                _race = null;
            }
            else if (races.Active > 0)
            {
                lastActive = races.Active;
                races.GetActiveIter(out var iter);
                var value = raceList.GetValue(iter, 2);
                _race = Races.GetRace(value.ToString());
            }

            strength.ResetRacialBonus();
            dexterity.ResetRacialBonus();
            constitution.ResetRacialBonus();
            intelligence.ResetRacialBonus();
            wisdom.ResetRacialBonus();
            charisma.ResetRacialBonus();
        };

        grid.Attach(races, 5, 7, 1, 1);

        box.PackStart(grid, true, true, 0);
        return box;
    }

    private AttributeWidget BuildAttributeWidget(string name)
    {
        var attributeName = name[..1].ToUpper() + name[1..];

        var totalScore = AbilityMin;
        var totalScoreLabel = Label.New($"{totalScore}");

        var abilityScore = AbilityMin;
        var abilityScoreButton = new SpinButton(AbilityMin, AbilityMax, 1);

        var abilityModifier = -1;
        var abilityModifierLabel = Label.New($"{abilityModifier}");

        var racialBonus = 0;
        var racialBonusButton = new SpinButton(-4, 4, 1);
        racialBonusButton.Value = 0;

        var pointCostLabel = Label.New($"{_costs[abilityScore]}");

        if (!IsCustomRace)
        {
            racialBonus = int.Parse(_race!.GetType().GetProperty($"{attributeName}Bonus")?.GetValue(_race)?.ToString() ?? "0");
        }

        void ValueChanged()
        {
            abilityScore = abilityScoreButton.ValueAsInt;
            racialBonus = racialBonusButton.ValueAsInt;

            totalScore = abilityScore + racialBonus;
            abilityModifier = (int) Math.Floor((totalScore - 10f) / 2f);

            totalScoreLabel.Text = $"{totalScore}";
            abilityModifierLabel.Text = $"{abilityModifier}";
            pointCostLabel.Text = $"{_costs[abilityScore]}";

            _abilityCosts[name] = _costs[abilityScore];

            PointCostChangedEvent?.Invoke(this);
        }

        abilityScoreButton.ValueChanged += (sender, args) => { ValueChanged(); };
        racialBonusButton.ValueChanged += (sender, args) => { ValueChanged(); };

        ValueChanged();

        void Reset()
        {
            abilityScoreButton.Value = AbilityMin;

            totalScore = abilityScore + racialBonus;
            abilityModifier = (int) Math.Floor((totalScore - 10f) / 2f);

            totalScoreLabel.Text = $"{totalScore}";
            abilityModifierLabel.Text = $"{abilityModifier}";
            pointCostLabel.Text = $"{_costs[abilityScore]}";

            PointCostChangedEvent?.Invoke(this);
        }

        void ResetRacialBonus()
        {
            racialBonusButton.Value = !IsCustomRace ? int.Parse(_race!.GetType().GetProperty($"{attributeName}Bonus")?.GetValue(_race)?.ToString() ?? "0") : 0;
            racialBonusButton.IsEditable = IsCustomRace;

            ValueChanged();
        }

        var attributeNameLabel = Label.New(Localizations.GetLocalizationForKey($"{ComponentLocalizationKey}.{name}.title"));
        var plusLabel = Label.New("+");
        var equalLabel = Label.New("=");

        attributeNameLabel.StyleContext.AddClass($"{CssClassPrefix}-attribute_label");
        plusLabel.StyleContext.AddClass($"{CssClassPrefix}-attribute_label");
        equalLabel.StyleContext.AddClass($"{CssClassPrefix}-attribute_label");
        totalScoreLabel.StyleContext.AddClass($"{CssClassPrefix}-attribute_label");
        abilityModifierLabel.StyleContext.AddClass($"{CssClassPrefix}-attribute_label");
        pointCostLabel.StyleContext.AddClass($"{CssClassPrefix}-attribute_label");

        var attr = new AttributeWidget
        {
            Widgets = new Widget[]
            {
                attributeNameLabel,
                abilityScoreButton,
                plusLabel,
                racialBonusButton,
                equalLabel,
                totalScoreLabel,
                abilityModifierLabel,
                pointCostLabel
            },
            Reset = Reset,
            ResetRacialBonus = ResetRacialBonus,
        };
        return attr;
    }
}