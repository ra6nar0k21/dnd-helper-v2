using Gtk;

namespace DnD_Helper.Components.List;

public class List : IComponent
{
    public string CssClassPrefix => "listpage";
    public string ComponentLocalizationKey => "page.list";
    public string ComponentTitleLocalizationKey => $"{ComponentLocalizationKey}.title";

    public Widget BuildWidget(Widget parent)
    {
        var notebook = new Notebook();
        notebook.AppendPage(Label.New(""), Label.New($"{ComponentLocalizationKey}.spells.title"));
        notebook.AppendPage(Label.New(""), Label.New($"{ComponentLocalizationKey}.items.title"));
        return notebook;
    }
}