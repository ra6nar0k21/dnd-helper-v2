using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DnD_Helper.Extensions;
using DnD_Helper.Utils;
using Gtk;
using Localization;


namespace DnD_Helper.Components.TranslationCreator;

public class TranslationCreator : IComponent
{
    public string CssClassPrefix => "translationcreator";
    public string ComponentLocalizationKey => "page.translationcreator";
    public string ComponentTitleLocalizationKey => $"{ComponentLocalizationKey}.title";

    private const string KeyBox = "key.box";
    private const string TranslationBox = "translation.box";
    private bool _edited = false;

    public Widget BuildWidget(Widget parent)
    {
        var window = (Window) parent;
        var box = new Box(Orientation.Vertical, 6);

        var scrollView = new ScrolledWindow();
        var translationList = new ListBox();
        translationList.SelectionMode = SelectionMode.None;
        var translation = Localizations.Current.Locals;
        foreach (var (k, v) in translation)
        {
            translationList.Add(BuildTranslationWidget(k, v));
        }

        scrollView.Add(translationList);
        box.PackStart(scrollView, true, true, 0);

        var loadDefaultButton = new Button
        {
            Label = Localizations.GetLocalizationForKey($"{ComponentLocalizationKey}.button.loaddefault")
        };
        var loadFileButton = new Button
        {
            Label = Localizations.GetLocalizationForKey($"{ComponentLocalizationKey}.button.loadfile")
        };
        var exportButton = new Button
        {
            Label = Localizations.GetLocalizationForKey($"{ComponentLocalizationKey}.button.exportfile")
        };
        loadDefaultButton.Clicked += (sender, args) =>
        {
            if (!_edited) return;

            var dialog = new MessageDialog(window, DialogFlags.Modal, MessageType.Warning, ButtonsType.None, "");
            dialog.Response += (o, responseArgs) =>
            {
                var result = responseArgs.ResponseId;
                if (result == ResponseType.Yes)
                {
                    foreach (var widget in translationList.Children)
                    {
                        var row = (ListBoxRow) widget;
                        row.FindChild<Entry>(TranslationBox).Text = string.Empty;
                    }
                }

                dialog.Destroy();
            };
            dialog.AddButton(Localizations.GetLocalizationForKey("dialog.yes"), ResponseType.Yes);
            dialog.AddButton(Localizations.GetLocalizationForKey("dialog.no"), ResponseType.No);

            dialog.Text = Localizations.GetLocalizationForKey($"{ComponentLocalizationKey}.dialog.unsaved.title");
            dialog.Run();
        };
        loadFileButton.Pressed += (sender, args) =>
        {
            var fileChooser = new FileChooserDialog($"{Localizations.GetLocalizationForKey("dialog.filechooser.title")} (*.lang)", window, FileChooserAction.Open);

            var filter = new FileFilter
            {
                Name = "Language"
            };
            filter.AddPattern("*.lang");
            fileChooser.AddFilter(filter);

            var openButton = fileChooser.AddButton(Localizations.GetLocalizationForKey("dialog.open"), ResponseType.Accept) as Button;
            var cancelButton = fileChooser.AddButton(Localizations.GetLocalizationForKey("dialog.cancel"), ResponseType.Cancel) as Button;

            fileChooser.SetCurrentFolder(Assembly.GetExecutingAssembly().Location[..Assembly.GetExecutingAssembly().Location.LastIndexOf(Path.DirectorySeparatorChar)]);

            void OpenFile()
            {
                var fileData = File.ReadAllLines(fileChooser.Filename);
                var local = LocalizationData.FromText(fileData);

                foreach (var widget in translationList.Children)
                {
                    var row = (ListBoxRow) widget;
                    var keyBox = row.FindChild<Entry>(KeyBox);

                    foreach (var (k, v) in local.Locals)
                    {
                        if (keyBox.Text != k) continue;

                        var transBox = row.FindChild<Entry>(TranslationBox);
                        transBox.Text = v;
                    }
                }
            }

            if (cancelButton != null) cancelButton.Clicked += (o, eventArgs) => { fileChooser.Destroy(); };
            if (openButton != null)
                openButton.Clicked += (o, eventArgs) =>
                {
                    if (!fileChooser.Filename.EndsWith(".lang"))
                    {
                        return;
                    }

                    OpenFile();
                    fileChooser.Destroy();
                };
            fileChooser.FileActivated += (o, eventArgs) =>
            {
                OpenFile();
                fileChooser.Destroy();
            };
            fileChooser.Run();
        };

        exportButton.Pressed += (sender, args) =>
        {
            var fileSave = new FileChooserDialog($"{Localizations.GetLocalizationForKey("dialog.filesave.title")} (*.lang)", window, FileChooserAction.Save);

            var filter = new FileFilter
            {
                Name = "Language"
            };
            filter.AddPattern("*.lang");
            fileSave.AddFilter(filter);

            var saveButton = fileSave.AddButton(Localizations.GetLocalizationForKey("dialog.save"), ResponseType.Accept) as Button;
            var cancelButton = fileSave.AddButton(Localizations.GetLocalizationForKey("dialog.cancel"), ResponseType.Cancel) as Button;
            if (cancelButton != null) cancelButton.Clicked += (o, eventArgs) => { fileSave.Destroy(); };
            if (saveButton != null)
                saveButton.Clicked += (o, eventArgs) =>
                {
                    if (fileSave.Filename == string.Empty)
                    {
                        return;
                    }

                    List<string> fileData = new();

                    foreach (var widget in translationList.Children)
                    {
                        var row = (ListBoxRow) widget;
                        var keyBox = row.FindChild<Entry>(KeyBox);

                        foreach (var (k, v) in Localizations.Current.Locals)
                        {
                            if (keyBox.Text != k) continue;

                            var transBox = row.FindChild<Entry>(TranslationBox);

                            if (transBox.Text == string.Empty) continue;

                            fileData.Add($"{k}:${transBox.Text}");
                        }
                    }

                    var file = FileUtils.NextAvailableFilename(fileSave.Filename);
                    if (!file.EndsWith(".lang"))
                    {
                        file = $"{file}.lang";
                    }

                    File.WriteAllLines(file, fileData);
                    fileSave.Destroy();
                };

            fileSave.Run();
        };

        var buttonBox = new Box(Orientation.Horizontal, 6);

        buttonBox.PackStart(loadDefaultButton, true, true, 0);
        buttonBox.PackStart(loadFileButton, true, true, 0);
        buttonBox.PackStart(exportButton, true, true, 0);

        box.PackStart(buttonBox, false, true, 0);
        return box;
    }

    private Widget BuildTranslationWidget(string key, string value)
    {
        var box = new Box(Orientation.Horizontal, 6);

        var keyBox = new Entry
        {
            Name = KeyBox,
            Text = key,
            IsEditable = false,
            CanFocus = false,
        };
        var translationBox = new Entry
        {
            Name = TranslationBox,
        };
        translationBox.TextInserted += (o, args) => { _edited = true; };

        box.PackStart(keyBox, true, true, 0);
        box.PackStart(translationBox, true, true, 0);

        return box;
    }
}