using Gtk;

namespace DnD_Helper.Components;

public interface IComponent
{
    public string CssClassPrefix { get; }
    public string ComponentLocalizationKey { get; }
    public string ComponentTitleLocalizationKey { get; }
    public Widget BuildWidget(Widget parent);
}