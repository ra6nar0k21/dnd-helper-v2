using System;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace DnD_Helper
{
    class old : Window
    {
        [UI] private readonly Label _label1 = null;
        [UI] private readonly Button _button1 = null;

        private int _counter;

        public old() : this(new Builder("MainWindow.glade"))
        {
        }

        private old(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
        {
            builder.Autoconnect(this);

            DeleteEvent += Window_DeleteEvent;
            _button1.Clicked += Button1_Clicked;
            _button1.Entered += (sender, args) =>
            {
                Console.Out.WriteLine("Test");
            };
        }

        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }

        private void Button1_Clicked(object sender, EventArgs a)
        {
            _counter++;
            _label1.Text = "Hello World! This button has been clicked " + _counter + " time(s).";
        }
    }
}