using System;
using System.IO;
using System.Text.Json.Nodes;
using Gdk;
using GLib;
using Gtk;
using Localization;
using Application = Gtk.Application;

namespace DnD_Helper
{
    internal static class Program
    {
        public static bool DebugMode = true;
        
        [STAThread]
        public static void Main(string[] args)
        {
            Resources.Init();
            Localizations.DefaultLocalization = LocalizationData.FromText(Resources.ReadInternal("Assets.default.lang").Split('\n'));
            Localizations.SelectLocalization("default");
            
            RaceLoader.LoadRaces(Resources.RaceFile, true);

            Application.Init();
            // Init Css styling
            var provider = new CssProvider();
            provider.LoadFromData(Resources.ReadInternal("Assets.style.css"));
            StyleContext.AddProviderForScreen(Screen.Default, provider, StyleProviderPriority.User);

            //Create the Window
            var myWin = new DnDHelperWindow();

            //Show Everything
            myWin.ShowAll();
            myWin.Present();

            Application.Run();
        }
    }
}